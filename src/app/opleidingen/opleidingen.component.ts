import { Component, OnInit } from '@angular/core';
import courses from  '../../assets/courses.json';

@Component({
  selector: 'app-opleidingen',
  templateUrl: './opleidingen.component.html',
  styleUrls: ['./opleidingen.component.scss']
})
export class OpleidingenComponent implements OnInit {
  public courses: any = courses;

  constructor() { }

  ngOnInit() {
  }

}
