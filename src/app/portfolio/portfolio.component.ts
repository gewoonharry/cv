import { Component, OnInit } from '@angular/core';
import portfolio from  '../../assets/portfolio.json';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss']
})

export class PortfolioComponent implements OnInit {
  public portfolio: any = portfolio;

  constructor() { }

  ngOnInit() {
  }

}
