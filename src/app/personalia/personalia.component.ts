import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-personalia',
  templateUrl: './personalia.component.html',
  styleUrls: ['./personalia.component.scss']
})
export class PersonaliaComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  dicht() {
    document.getElementById('toggle').click();
  }

}
