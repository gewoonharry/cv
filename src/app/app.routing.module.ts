import { NgModule } from '@angular/core';
import { OpleidingenComponent } from './opleidingen/opleidingen.component';
import { Routes, RouterModule } from '@angular/router';
import { PersonaliaComponent } from './personalia/personalia.component';
import { WerkervaringComponent } from './werkervaring/werkervaring.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { NotFoundComponent } from './not-found/not-found.component';

const appRoutes: Routes = [
  { path: '', redirectTo: '/personalia', pathMatch: 'full' },
  { path: 'personalia', component: PersonaliaComponent },
  { path: 'opleidingen', component: OpleidingenComponent },
  { path: 'werkervaring', component: WerkervaringComponent },
  { path: 'portfolio', component: PortfolioComponent },
  { path: '404', component: NotFoundComponent },
  { path: '**', redirectTo: '/404' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule]
})

export class AppRoutingModule {

}
